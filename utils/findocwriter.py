# -*- coding: utf-8 -*-
from docx import Document
from docx.shared import Pt

from monkeypatch import add_justified_paragraph, add_centered_picture


class FinDocument(object):

    def __new__(cls):
        document = Document()
        style = document.styles['Normal']
        font = style.font
        font.name = 'Times New Roman'
        font.size = Pt(14)

        # monkey patching
        for func in [add_centered_picture, add_justified_paragraph]:
            setattr(document.__class__, func.__name__, func)

        return document


if __name__ == '__main__':
    # ----------------------
    #      Test example
    # ----------------------
    document = FinDocument()

    document.add_heading('Document Title', 0)

    text = """Lorem ipsum dolor sit amet, electram scripserit eum ad, cu movet
    detracto tincidunt duo. Doming vivendum lucilius et sed. Soluta insolens
    legendos no usu. Ex illud etiam philosophia mel, option assentior ut sit,
    sed verear necessitatibus no. Exerci eruditi fabulas sed id, modus oratio
    antiopam cum eu.
    """

    document.add_justified_paragraph(text)

    document.add_justified_paragraph(text)

    document.add_centered_picture(
        '2016-05-10T20:05:46.png',
        u'Рис. 1.1 Показники рентабельності виробництва малого бізнесу')

    document.save('demo.docx')
