from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Inches, Cm

from helpers import paragraph_formater


def add_justified_paragraph(self, text):
    paragraph = self.add_paragraph(paragraph_formater(text))
    paragraph.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY

    # tabulated
    paragraph.paragraph_format.first_line_indent = Inches(0.5)
    paragraph.paragraph_format.line_spacing = Cm(1)
    return paragraph


def add_centered_picture(self, pname, text=None):
    pic = self.add_picture(pname, width=Inches(5))
    last_paragraph = self.paragraphs[-1]
    last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

    if text:
        paragraph = self.add_paragraph(paragraph_formater(text))
        paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
    return pic
