# -*- coding: utf-8 -*-
import __future__

import importlib
import re
import functools
import matplotlib.pyplot as plt
import numpy
import pandas
from inspect import getdoc
from collections import Counter, OrderedDict
from keymapper import GROUP_MAP_COMPATIBILITY


pandas.set_option('display.float_format', '{:.4f}'.format)
pandas.set_option('display.max_colwidth', 70)
pandas.set_option('display.width', 150)


class Settings(object):

    def __init__(self, settings_name='settings'):
        self.settings_module = importlib.import_module(settings_name)
        for prop in dir(self.settings_module):
            if not prop.startswith('__'):
                setattr(self, prop, getattr(self.settings_module, prop, None))

    @property
    def _mixins(self):
        return [
            getattr(importlib.import_module(mod), cls)
            for (mod, cls) in (
                mixin.rsplit(".", 1)
                for mixin in getattr(
                    self.settings_module, 'ANALIZER_MIXINS', tuple()))]


settings = Settings()


class MixinsMetaPatch(type):

    def __new__(cls, name, bases, attrs):
        if settings.ANALIZER_MIXINS:
            bases = tuple(settings._mixins)
        return super(MixinsMetaPatch, cls).__new__(cls, name, bases, attrs)


def paragraph_formater(text):
    return ' '.join(text.splitlines()).strip()


def summary(df, fn=numpy.sum, axis=0, name='Total',
            table_class_prefix='dataframe-summary'):
    """Append a summary row or column to DataFrame.

    Input:
    ------
    df : Dataframe to be summarized
    fn : Summary function applied over each column
    axis : Axis to summarize on (1: by row, 0: by column)
    name : Index or column label for summary
    table_class_prefix : Custom css class for dataframe

    Returns:
    --------
    Dataframe with applied summary.

    """
    total = df.apply(fn, axis=axis).to_frame(name)

    table_class = ""

    if axis == 0:
        total = total.T
        table_class = "{}-row".format(table_class_prefix)
    elif axis == 1:
        table_class = "{}-col".format(table_class_prefix)

    out = pandas.concat([df, total], axis=axis)
    # Patch to_html function to use custom css class
    out.to_html = functools.partial(out.to_html, classes=table_class)
    return out


def dict2list(dict1):
    return [v for v in OrderedDict(dict1).itervalues()]


def multiply_dicts(dict1, dict2):
    return {k: v * dict2[k] for k, v in dict1.items() if k in dict2}


def devide_dicts(dict1, dict2):
    return {k: v / float(dict2[k]) for k, v in dict1.items() if k in dict2}


def substract_dicts(dict1, dict2):
    return {k: dict1[k] - dict2.get(k, 0) for k in dict1.keys()}


def sum_dicts(dict1, dict2):
    return {k: v + dict2[k] for k, v in dict1.items() if k in dict2}


def dict_to_persent(dict1):
    return {k: dict1[k] * 100 for k in dict1.keys()}


def lazyproperty(fn):
    attr_name = '_lazy_' + fn.__name__

    @property
    @functools.wraps(fn)
    def _lazyproperty(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)
    return _lazyproperty


def lazyformprop(fn):
    attr_name = '_lazyformprop_' + fn.__name__

    @property
    @functools.wraps(fn)
    def _lazyformprop(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, eval_form(self, fn(self)))
        return getattr(self, attr_name)
    return _lazyformprop


def joined_form_values(form, fname, group_map, gnames):
    data = []
    for gname in gnames:
        new_gname = group_map[fname].get(gname, 'nan')
        if isinstance(new_gname, list):
            data.append(joined_form_values(form, fname, group_map, new_gname))
        else:
            if new_gname == 'nan':
                data.append(form[gname])
            else:
                data.append(form[new_gname])
    return reduce(lambda x, y: MultiFormDict(x) + MultiFormDict(y), data)


def get_group_value(year, form, gname, fname):
    fname, gname, group_map = str(fname), str(gname), None
    if year <= 2010:
        group_map = GROUP_MAP_COMPATIBILITY[2010]
    if year >= 2014:
        group_map = GROUP_MAP_COMPATIBILITY[2014]
    if group_map:
        new_gname = group_map[fname].get(gname, 'nan')
        if isinstance(new_gname, list):
            return joined_form_values(form, fname, group_map, new_gname)
        if new_gname != 'nan':
            return form[new_gname]
    return form[gname]


def eval_form(instance, string):
    """TODO: Refactor this.
    Its very complicated, need to try to make more simplier.
    """
    is_form_2 = False
    splited_query = re.findall(r"(f\d+_r\d+|[\-+*/().]|\d+)", string)
    upd_data = {"3": {}, "4": {}}
    result_data = MultiFormDict({})
    if not splited_query:
        return {}
    for i, el in enumerate(splited_query):
        if re.match(r'^f\d+_r\d+$', el):
            if "f2_" in el and not is_form_2:
                is_form_2 = True
                del upd_data["4"]
            fname, gname = re.findall(r"\d+", el)
            splited_query[i] = "{%s}" % el
            form = getattr(instance, "form_%s" % fname)
            form_year = instance._meta["year"]
            form_row = get_group_value(form_year, form, gname, fname)
            if is_form_2:
                if "f2_" in el:
                    upd_data['3'][el] = form_row["3"]
                elif "f1_" in el:
                    upd_data['3'][el] = numpy.mean([
                        form_row["3"], form_row["4"]
                    ])
                continue
            for k in form[gname].iterkeys():
                upd_data[k][el] = form_row[k]
    formated_str = "".join(splited_query)
    for k, v in upd_data.iteritems():
        try:
            result_data[k] = eval(
                compile(
                    formated_str.format(**v), '<string>',
                    'eval', __future__.division.compiler_flag))
        except ZeroDivisionError:
            get_zero_key = [
                zerok for zerok, zerov in v.iteritems() if zerov == 0][0]
            raise ZeroDivisionError(
                "Group %s is incorrect. Can't be zero."
                "Check your form to continue operation." % str(get_zero_key))
    if is_form_2:
        return result_data['3']
    return result_data


def get_class_methods_names(klass):

    return [
        method for method in dir(klass)
        if not method.startswith('__') and not method.startswith('_') and not
        method.startswith('table_') and not method.endswith('_info') and not
        method.isupper()
    ]


def show_figure(fig):

    # create a dummy figure and use its
    # manager to display "fig"

    dummy = plt.figure()
    new_manager = dummy.canvas.manager
    new_manager.canvas.figure = fig
    fig.set_canvas(new_manager.canvas)


def get_properties_docstrings(klass):
    return [
        getdoc(getattr(klass, fname)).strip()
        for fname in get_class_methods_names(klass)]


def to_console_table(instance, klass, exclude_names=None, report_dates=None):

    def date_to_col(instance, mname):
        if len(report_dates) == 1:
            return [getattr(instance, mname)]
        return OrderedDict(getattr(instance, mname)).values()

    _headers = []
    data = []

    if isinstance(report_dates, str):
        report_dates = [report_dates]

    if exclude_names is None:
        exclude_names = []

    if report_dates is None:
        report_dates = instance.report_dates or []

    for mname in get_class_methods_names(klass):
        _headers.append(getdoc(getattr(klass, mname)).strip())
        data.append(date_to_col(instance, mname))

    return pandas.DataFrame(
        data, index=_headers, columns=report_dates)


def show_only(df, rnames=None):
    if rnames is None:
        rnames = []

    return df.iloc[
        [df.index.get_loc(
            (lambda rn: rn.encode('utf-8')
             if isinstance(rn, unicode) else rn)(rn)) for rn in rnames]]


class MultiFormDict(Counter):

    DEFAULT_VALUE = {"3": 0, "4": 0}

    def __getitem__(self, key):
        return self.get(key, self.DEFAULT_VALUE)

    def _dict_handle(self, value):
        if isinstance(value, dict):
            return value
        elif isinstance(value, str):
            return {k: int(value) for k in self.iterkeys() if value.isdigit()}
        elif isinstance(value, int) or isinstance(value, float):
            return {k: value for k in self.iterkeys()}
        # TODO: add handles of str, unicode
        return self.DEFAULT_VALUE

    def __add__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(sum_dicts(self, _dict))

    def __radd__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(sum_dicts(_dict, self))

    def __sub__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(substract_dicts(self, _dict))

    def __rsub__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(substract_dicts(_dict, self))

    def __mul__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(multiply_dicts(self, _dict))

    def __rmul__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(multiply_dicts(_dict, self))

    def __div__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(devide_dicts(self, _dict))

    def __rdiv__(self, value):
        _dict = self._dict_handle(value)
        return MultiFormDict(devide_dicts(_dict, self))
