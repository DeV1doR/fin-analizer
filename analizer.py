# -*- coding: utf-8 -*-
import os
import json
import pandas as pd
from inspect import getdoc, getmro

import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams


from utils import (
    MultiFormDict, show_only, get_properties_docstrings, to_console_table,
    FinDocument, MixinsMetaPatch, settings)
from mixins import (
    EntCapRentabilityMixin, EntSaleRentabilityMixin, EntProdRentabilityMixin)


rcParams.update({
    'figure.autolayout': True,
    'xtick.major.pad': 8,
    'ytick.major.pad': 8
})
font = {'family': 'Droid Sans',
        'weight': 'normal'}
rc('font', **font)


class FinAnalizer(object):

    __metaclass__ = MixinsMetaPatch

    def __init__(self, data, detailed=False):
        for k, v in data.iteritems():
            if not k.startswith("_"):
                setattr(self, "form_%s" % k, MultiFormDict(v))
            else:
                setattr(self, k, v)

        self.report_dates = (
            "%s-01-01" % self._meta["year"]
            if detailed else "31.12.%s" % (self._meta["year"] - 1),
            "%s-12-31" % self._meta["year"]
            if detailed else "31.12.%s" % self._meta["year"]
        )
        self.fin_result_date = (
            "%s-12-31" % self._meta["year"]
            if detailed else "31.12.%s" % self._meta["year"]
        )

    @classmethod
    def from_data(cls, fname):
        with open(fname) as file:
            data = json.load(file)
        return cls(data)

    @classmethod
    def from_datas(cls, fnames=None):
        if fnames is None:
            try:
                fnames = os.listdir(settings.LOADER_FOLDER)
            except OSError:
                fnames = []
        return [cls.from_data(fname) for fname in fnames]

    @staticmethod
    def save_plot_from_table(table, klass_analiz=None, title=None,
                             excludes=None, rnames=None, kind='line',
                             img_format='png', rotate_labels=True, **kwargs):

        def start_setup(rnames, excludes, title):
            if rnames is None:
                rnames = []

            if excludes is None:
                excludes = []

            if klass_analiz is not None:
                if not rnames:
                    rnames = [
                        rname for rname in
                        get_properties_docstrings(klass_analiz)
                        if rname not in excludes]
                if not title:
                    title = getdoc(klass_analiz).splitlines()[0].strip()
                    try:
                        title = title.decode('utf-8')
                    except:
                        pass

            if title is None:
                title = 'Custom title'

            return rnames, title

        def table_with_kind_configs(table):
            if kind == 'line':
                table = table.T
                kwargs.update({
                    "style": 'o-'
                })

            if kind == 'bar':
                table = table.T

            if rotate_labels:
                kwargs.update({
                    "rot": 0,
                    "alpha": 0.75
                })
            return table

        def change_bar_legend(ploted):
            svkwargs = {}
            if kind == 'bar':
                handles, labels = ploted.get_legend_handles_labels()
                lgd = ploted.legend(
                    handles, labels,
                    loc='upper center', bbox_to_anchor=(0.5, -0.1))
                svkwargs.update({
                    'bbox_extra_artists': (lgd, ),
                    'bbox_inches': 'tight'
                })
                plt.axhline(0, color='black')
            return svkwargs

        rnames, title = start_setup(rnames, excludes, title)

        table = table_with_kind_configs(show_only(table, rnames))

        ploted = table.plot(
            kind=kind, title=title, colormap=plt.cm.RdYlGn, **kwargs)

        svkwargs = change_bar_legend(ploted)

        if not os.path.exists(settings.IMAGE_FOLDER):
            os.makedirs(settings.IMAGE_FOLDER)

        figure = ploted.get_figure()
        figure.set_size_inches(9.25, 5.25)
        figure.savefig(
            ''.join([settings.IMAGE_FOLDER, title, '.', img_format]),
            **svkwargs)

    @classmethod
    def to_table(cls, analizers, nform=1, rnames=None):
        """Construct joined table from all analisys.

        =======================================================================
        Args:

            analizers: list of instances or file names, which to be imported
                       in program, @instance or @list[@str]

            nform:     number of form to table view, @int
                       (default: 1, supported: 1, 2)

            rnames:    list of names or string name of row(s), which will be
                       presented in table, @list[@str(or @unicode)] or
                       @str(or @unicode) (default: [])
        Returns:

            pandas.DataFrame @instance

        =======================================================================
        Examples:

            [In]>> FinAnalizer.build_joined_f1_table(['/path/to/file.json', ])
            [Out]>>                            2010   2011   2012   2013
            Коефіцієнт фінансової залежності 4.3065 4.4797 4.5091 4.4880

            or

            [In]>> FinAnalizer.build_joined_f1_table([inst1, inst2, etc...])

        =======================================================================
        """
        merged_frames = []

        if isinstance(rnames, str):
            rnames = [rnames]
        elif isinstance(rnames, unicode):
            rnames = [rnames.encode('utf-8')]

        if rnames is None:
            rnames = []

        exclude_classes = [
            EntCapRentabilityMixin, EntSaleRentabilityMixin,
            EntProdRentabilityMixin]

        def joiner(left, right):
            if nform == 2:
                return left.join(right)
            return pd.merge(left, right, on=right.columns[0], right_index=True)

        def get_analizer(analizer):
            if not isinstance(analizer, cls):
                analizer = cls.from_data(analizer)
            return analizer

        def switch_table_form(analizer, klass):
            if nform == 2:
                return to_console_table(
                    analizer, klass, report_dates=analizer.fin_result_date)
            return to_console_table(analizer, klass)

        iter_klasses = exclude_classes
        if nform == 1:
            iter_klasses = [
                klass for klass in getmro(cls)[1:-1]
                if klass not in exclude_classes]

        for parent_klass in iter_klasses:
            tables = [
                switch_table_form(get_analizer(analizer), parent_klass)
                for analizer in analizers]

            if nform == 1:
                try:
                    tables = tables[1:]
                except IndexError:
                    pass

            merged_frames.append(
                reduce(lambda left, right: joiner(left, right), tables))

        merged_frames = pd.concat(merged_frames)
        if rnames:
            merged_frames = merged_frames.iloc[
                [merged_frames.index.get_loc(
                    (lambda rn: rn.encode('utf-8')
                        if isinstance(rn, unicode) else rn)(rn))
                    for rn in rnames]]

        return merged_frames.groupby(merged_frames.index).first()

    @staticmethod
    def to_docx(docx_name='graph.docx'):
        """Save data of all pictures in docx file.

        =======================================================================
        Args:

            docx_name: name of file to save, @str

        Returns:

            @void func

        =======================================================================
        """
        document = FinDocument()
        for file_name in os.listdir(settings.IMAGE_FOLDER):
            with open(
                unicode(settings.IMAGE_FOLDER + file_name, 'utf-8')
            ) as file:
                document.add_centered_picture(file)
        document.save(docx_name)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return u"<{0} for {1}>".format(
            self.__class__.__name__, self._meta["year"])


if __name__ == '__main__':
    # analizer = FinAnalizer.from_data("data/fin_rep_2010.json")
    # print analizer.products_profitablity
    # print analizer.general_products_profitablity
    # analizer.show_tables(splited=False)
    analizers_data = [
        "data/fin_rep_2009.json", "data/fin_rep_2010.json",
        "data/fin_rep_2011.json", "data/fin_rep_2012.json",
        "data/fin_rep_2013.json", "data/fin_rep_2014.json"
    ]
    print FinAnalizer.to_table(analizers_data)
    print FinAnalizer.to_table(analizers_data, nform=2)
