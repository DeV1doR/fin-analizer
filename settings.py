# -*- coding: utf-8 -*-

# matplotlib picture save folder
IMAGE_FOLDER = 'images/'

# path for loading data
# NotImplemented
LOADER_FOLDER = 'data/'

# add mixins for table view
ANALIZER_MIXINS = [
    'mixins.enterprise_liquidity.EntCoefLiquidityMixin',
    'mixins.enterprise_stability.EntStabilityMixin', 
    'mixins.enterprise_stability.EntThreeCompStabilityMixin',
    'mixins.enterprise_rentability.EntCapRentabilityMixin',
    'mixins.enterprise_rentability.EntSaleRentabilityMixin',
    'mixins.enterprise_rentability.EntProdRentabilityMixin',
    'mixins.enterprise_cred_debt.EntCredDebtMixin',
    'mixins.enterprise_state_property.EntStatePropMixin',
    'mixins.data_from_form.DataForm1Mixin',
]
