# -*- coding: utf-8 -*-
from utils import lazyformprop


class EntCapRentabilityMixin(object):
    """Аналіз рентабельності капіталу.

    Показники рентабельності капіталу малого підприємства.
    """

    @lazyformprop
    def return_on_total_capital(self):
        """Рентабельність сукупного капіталу, %
        """
        return "100 * f2_r150 / (f1_r380 + f1_r480 + f1_r620)"

    @lazyformprop
    def equity_efficiency(self):
        """Рентабельність власного капіталу, %
        """
        return "100 * f2_r150 / f1_r380"

    @lazyformprop
    def return_on_equity(self):
        """Рентабельність основного капіталу, %
        """
        return "100 * f2_r150 / f1_r080"

    @lazyformprop
    def return_on_operating_capital(self):
        """Рентабельність операційного капіталу, %
        """
        return "100 * f2_r150 / (f1_r080 + f1_r260)"


class EntSaleRentabilityMixin(object):
    """Аналіз рентабельності продажу.

    Показники рентабельності продажу малого підприємства.
    """

    @lazyformprop
    def return_on_sales(self):
        """Рентабельність реалізованої продукції, %
        """
        return "100 * f2_r150 / f2_r030"

    @lazyformprop
    def return_on_revenue(self):
        """Рентабельність доходів, %
        """
        return "100 * f2_r150 / f2_r070"


class EntProdRentabilityMixin(object):
    """Аналіз рентабельності виробництва.

    Показники рентабельності виробництва малого підприємства.
    """

    @lazyformprop
    def products_profitablity(self):
        """Рентабельність виробництва продукції, %
        """
        return "100 * f2_r150 / f2_r080"

    @lazyformprop
    def general_products_profitablity(self):
        """Рентабельність продукції в цілому, %
        """
        return "100 * f2_r150 / f2_r120"
