# -*- coding: utf-8 -*-
from utils import lazyproperty, lazyformprop


class EntStabilityMixin(object):
    """Аналіз фінансової стійкості.

    Показники фін. стійкості підприємства.
    """

    @lazyformprop
    def financial_autonomy_coefficient(self):
        """Коефіцієнт фінансової автономії
        """
        return "f1_r380 / f1_r280"

    @lazyformprop
    def net_assets(self):
        """Чисті активи
        """
        return "f1_r160 - f1_r530"

    @lazyformprop
    def loan_capital_coefficient(self):
        """Коефіцієнт фінансової залежності
        """
        return "f1_r280 / f1_r380"

    @lazyformprop
    def equity_flexibility_coefficient(self):
        """Коефіцієнт маневреності власного капіталу
        """
        return "(f1_r380 + f1_r480 - f1_r080) / (f1_r380 + f1_r480)"

    @lazyformprop
    def financial_risk_coefficient(self):
        """Коефіцієнт фінансового ризику
        """
        return "(f1_r500 + f1_r510) / f1_r380"

    @lazyformprop
    def mobility_equity_coefficient(self):
        """Коефіцієнт маневреності власних засобів
        """
        return "(f1_r380 - f1_r080) / f1_r380"

    @lazyformprop
    def working_capital_flexibility_coefficient(self):
        """Коефіцієнт маневреності робочого капіталу
        """
        return "(f1_r260 - f1_r620) / f1_r380"

    @lazyformprop
    def financial_stability_coefficient(self):
        """Коефіцієнт фінансової стабільності
        """
        return "f1_r380 / (f1_r420 + f1_r480 + f1_r620 + f1_r630)"

    @lazyformprop
    def financial_activity_coefficient(self):
        """Коефіцієнт фінансової активності
        """
        return "(f1_r480 + f1_r620) / f1_r380"

    @lazyformprop
    def involve_equity_coefficient(self):
        """Коеф. залученого і власного капіталу
        """
        return "(f1_r420 + f1_r480 + f1_r620 + f1_r630) / f1_r380"

    @lazyformprop
    def concentration_equity_coefficient(self):
        """Коеф. концентрації власного капіталу
        """
        return "f1_r380 / (f1_r080 + f1_r260 + f1_r270)"

    @lazyformprop
    def concentration_debt_coefficient(self):
        """Коеф. концентрації позикового капіталу
        """
        return "(f1_r430 + f1_r480 + f1_r620 + f1_r630) " \
            "/ (f1_r080 + f1_r260 + f1_r270)"


class EntThreeCompStabilityMixin(object):
    """Трикомпонентний показник фін. стійкості підприємства.
    """

    @lazyformprop
    def own_working_capital(self):
        """Власні оборотні кошти (ВОК)
        """
        return "f1_r260 - f1_r620"

    @lazyformprop
    def long_working_sources(self):
        """Довгострокові оборотні джерела (ВДК)
        """
        return "f1_r380 - f1_r080 + f1_r275 + f1_r480"

    @lazyformprop
    def total_source_value_formation(self):
        """Загальна величина осн. джерел формування (ОДК)
        """
        return "f1_r380 - f1_r080 + f1_r275 + f1_r480 + f1_r500"

    @lazyproperty
    def _three_component_index(self):
        """Трикомпонентний показник
        """
        _index = {"3": [], "4": []}
        for k in _index.iterkeys():
            if self.own_working_capital[k] > 0:
                _index[k].append(1)
            elif self.own_working_capital[k] < 0:
                _index[k].append(0)
            if self.long_working_sources[k] > 0:
                _index[k].append(1)
            elif self.long_working_sources[k] < 0:
                _index[k].append(0)
            if self.total_source_value_formation[k] > 0:
                _index[k].append(1)
            elif self.total_source_value_formation[k] < 0:
                _index[k].append(0)
        return _index

    @lazyproperty
    def _financial_stability_type(self):
        """Тип фінансової стійкості
        """
        _type = {}
        for k, v in self._three_component_index.iteritems():
            one_c = v.count(1)
            if one_c == 3:
                _type[k] = u"Має абсолютну фінансову стійкість."
            elif one_c == 2:
                _type[k] = u"Має нормальна фінансову стійкість."
            elif one_c == 1:
                _type[k] = u"Має хітливе фінансове становище."
            else:
                _type[k] = u"Має кризовий фінансовий стан."
        return _type
