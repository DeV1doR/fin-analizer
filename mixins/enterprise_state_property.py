# -*- coding: utf-8 -*-
from utils import lazyformprop


class EntStatePropMixin(object):
    """Аналіз майнового становища.
    """

    @lazyformprop
    def real_property_value_coefficient(self):
        """Коеф. реальної вартості майна
        """
        return "(f1_r030 + f1_r100 + f1_r130) / f1_r280"

    @lazyformprop
    def working_capital_provide_coefficient(self):
        """Коеф. забезпечення власними оборотними коштами
        """
        return "f1_r230 / f1_r260"
