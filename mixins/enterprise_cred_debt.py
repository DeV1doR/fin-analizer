# -*- coding: utf-8 -*-
from utils import lazyformprop


class EntCredDebtMixin(object):
    """Аналіз дебіторської та кредиторської заборгованості.
    """

    @lazyformprop
    def cred_level(self):
        """Рівень кредиторської заборгованості
        """
        return "f1_r530 / f1_r260"

    @lazyformprop
    def debt_level(self):
        """Рівень дебіторської заборгованості
        """
        return "f1_r160 / f1_r260"
