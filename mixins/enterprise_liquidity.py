# -*- coding: utf-8 -*-
from utils import lazyformprop


class EntCoefLiquidityMixin(object):
    """Аналіз ліквідності та платоспроможності підприємства.

    Показники платоспроможності і ліквідності підприємства
    за допомогою фін. коефіцієнтів.
    """

    @lazyformprop
    def coverage_coefficient(self):
        """Коеф. (покриття) поточної ліквідності
        """
        return "f1_r260 / f1_r620"

    @lazyformprop
    def absolute_liquidity_coefficient(self):
        """Коеф. абсолютної ліквідності
        """
        return "(f1_r220 + f1_r230 + f1_r240) / f1_r620"

    @lazyformprop
    def quick_liquidity_coefficient(self):
        """Коеф. швидкої ліквідності
        """
        return "(f1_r230 + f1_r240 + f1_r220 + f1_r210 + f1_r160 + f1_r160)" \
            " / f1_r620"
