# -*- coding: utf-8 -*-
from utils import lazyformprop


class DataForm1Mixin(object):
    """Данні з балансу.
    """

    @lazyformprop
    def equity(self):
        """Власний капітал
        """
        return "f1_r380"

    @lazyformprop
    def current_liabilities(self):
        """Поточні зобов'язання
        """
        return "f1_r620"


class DataForm2Mixin(object):
    """Данні зі звіту про фін. результати.
    """
    pass
