# -*- coding: utf-8 -*-
from analizer import FinAnalizer
from mixins import (
    EntCoefLiquidityMixin, EntStabilityMixin, EntThreeCompStabilityMixin,
    EntCapRentabilityMixin, EntSaleRentabilityMixin, EntProdRentabilityMixin,
    EntCredDebtMixin, EntStatePropMixin, DataForm1Mixin
)


class FinAnalizerPloter(object):

    engine_model = FinAnalizer

    @classmethod
    def from_datas(cls, fnames=None):
        if not hasattr(cls, '_loaded_data'):
            cls._loaded_data = cls.engine_model.from_datas(fnames)
            cls._build_forms()
        return cls._loaded_data

    @classmethod
    def _build_forms(cls):
        for x in [1, 2]:
            setattr(cls, 'F{0}_table'.format(x),
                    cls.engine_model.to_table(cls._loaded_data, nform=x))

    @classmethod
    def plot_entcoefliquidity(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, EntCoefLiquidityMixin)

    @classmethod
    def plot_entcaprentability(cls):
        cls.engine_model.save_plot_from_table(
            cls.F2_table, EntCapRentabilityMixin, kind='bar')

    @classmethod
    def plot_entsalerentability(cls):
        cls.engine_model.save_plot_from_table(
            cls.F2_table, EntSaleRentabilityMixin, kind='bar')

    @classmethod
    def plot_entprodrentability(cls):
        cls.engine_model.save_plot_from_table(
            cls.F2_table, EntProdRentabilityMixin, kind='bar')

    @classmethod
    def plot_structure_capital(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, DataForm1Mixin, kind='bar',
            title=u'Структура капіталу в динаміці.',
            rnames=[
                'Власний капітал',
                'Поточні зобов\'язання'],
            stacked=True)

    @classmethod
    def plot_entcompstability_a_s_r(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, EntStabilityMixin, kind='bar',
            title=u'Аналіз фінансової стійкості за автономією, '
            u'стабільності, ризику та маневреності власного капіталу.',
            rnames=[
                'Коефіцієнт фінансової автономії',
                'Коефіцієнт фінансової стабільності',
                'Коефіцієнт фінансового ризику',
                'Коефіцієнт маневреності власного капіталу'])

    @classmethod
    def plot_entthreecompstability(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, EntThreeCompStabilityMixin, kind='bar')

    @classmethod
    def plot_entstateprop(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, EntStatePropMixin, kind='bar')

    @classmethod
    def plot_entcreddebt(cls):
        cls.engine_model.save_plot_from_table(
            cls.F1_table, EntCredDebtMixin)

    @classmethod
    def plot_all(cls):
        for fname in dir(cls):
            if fname.startswith('plot_') and not fname.endswith('_all'):
                getattr(cls, fname)()


if __name__ == '__main__':
    FinAnalizerPloter.from_datas([
        "data/fin_rep_2009.json", "data/fin_rep_2010.json",
        "data/fin_rep_2011.json", "data/fin_rep_2012.json",
        "data/fin_rep_2013.json", "data/fin_rep_2014.json",
        "data/fin_rep_2015.json"
    ])
    FinAnalizerPloter.plot_all()
