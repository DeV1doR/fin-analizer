# -*- coding: utf-8 -*-
from fabric.api import local

from utils import settings


def cleanpyc(f):

    def wrapper(*args, **kwargs):
        local('find . -name "*.pyc" -exec rm -rf {} \;')
        f(*args, **kwargs)

    return wrapper

@cleanpyc
def tables():
    """Show analizer tables.
    """
    local('python analizer.py')

@cleanpyc
def plot(folder=settings.IMAGE_FOLDER):
    """Plots graphs and save in folder.
    """
    local('rm -R {0}'.format(folder))
    local('python graph.py')

def freeze(req_name='requirements.txt'):
    local('pip freeze > {0}'.format(req_name))
